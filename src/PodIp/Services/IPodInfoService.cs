using K8s.PodIp.Models;

namespace K8s.PodIp.Services
{
    public interface IPodInfoService
    {
        PodInfo GetPodInfo();
    }
}