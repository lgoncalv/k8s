using System;
using System.Collections.Generic;
using System.Drawing;
using K8s.PodIp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Caching.Memory;

namespace K8s.PodIp.Services
{
    public class PodInfoService : IPodInfoService
    {
        private readonly HttpContext _httpContext;
        private readonly IMemoryCache _memoryCache;

        public PodInfoService(IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _memoryCache = memoryCache;
        }

        public PodInfo GetPodInfo()
        {
            var httpConnectionFeature = _httpContext.Features.Get<IHttpConnectionFeature>();
            var podIp = httpConnectionFeature != null ? httpConnectionFeature.LocalIpAddress.MapToIPv4().ToString() : "Unknown";

            return new PodInfo
            {
                Ip = podIp,
                Color = GetPodColor()
            };
        }

        private string GetPodColor()
        {
            const string CacheKey = "PodColor";
            string color;
            if(_memoryCache.TryGetValue(CacheKey, out color))
                return color;

            var podColor = GetRandomColor();
            _memoryCache.Set(CacheKey, podColor, TimeSpan.FromDays(1));
            return podColor;
        }

        private string GetRandomColor()
        {
            var random = new Random();
            var color = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
            return $"#{color.R:X2}{color.G:X2}{color.B:X2}";
        }
    }
}