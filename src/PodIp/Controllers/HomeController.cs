using K8s.PodIp.Models;
using K8s.PodIp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace K8s.PodIp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPodInfoService _podInfoService;

        public HomeController(IPodInfoService pageInfoService)
        {
            _podInfoService = pageInfoService;
        }

        public ActionResult Index()
        {
            return View(_podInfoService.GetPodInfo());
        }
    }
}