namespace K8s.PodIp.Models
{
    public class PodInfo
    {
        public string Ip {get;set;}
        public string Color {get;set;}
    }
}