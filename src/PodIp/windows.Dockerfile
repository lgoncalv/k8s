# Build
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish -o /publish

# Runtime
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2.5-nanoserver-1803
WORKDIR /app
COPY --from=build-env /publish .
ENTRYPOINT [ "dotnet", "PodIp.dll" ]